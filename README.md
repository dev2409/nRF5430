## steps to recreate issue
```bash
cd nrf5340
west update
```

```bash
west build -b nrf5340dk_nrf5340_cpuapp_ns . -DCONF_FILE="prj.conf overlay-ot.conf overlay-tls.conf overlay-log.conf" --pristine
```
