/* from scratch file from Jan

feel free license
this application starts a DTLS client sending a COAPS message to a specified server via OpenThread.

*/

//includes

#include <logging/log.h>
LOG_MODULE_REGISTER(net_echo_client_sample, LOG_LEVEL_DBG);

#include <zephyr.h>
#include <errno.h>
#include <stdio.h>
#include <fcntl.h>

#include <net/socket.h>
#include <net/tls_credentials.h>
#include <net/coap.h>

#include <net/openthread.h>
#include "dummy_psk.h"

#include "mbedtls/debug.h"


//defines
#define PSK_TAG 2
#define PEER_PORT 5684
#define TLS_PEER_HOSTNAME ""
#define MAX_COAP_MSG_LEN 1024





//declarations
static const char * const test_path[] = { "crts", NULL };
struct data {
	const char *proto;

	struct {
		int sock;
		/* Work controlling udp data sending */
		struct k_work_delayable recv;
		struct k_work_delayable transmit;
		uint32_t expecting;
		uint32_t counter;
		uint32_t mtu;
	} udp;

	struct {
		int sock;
		uint32_t expecting;
		uint32_t received;
		uint32_t counter;
	} tcp;
};
mbedtls_ssl_config _ssl_conf;

 /**

     * Debug callback for mbed TLS

     */

    static void my_debug(void *ctx, int level, const char *file, int line, const char *str)
    {
        const char *p, *basename;
        (void) ctx;
        /* Extract basename from file */
        for(p = basename = file; *p != '\0'; p++) {
            if(*p == '/' || *p == '\\') {
                basename = p + 1;
            }
        }
        LOG_INF("%s:%04d: |%d| %s", basename, line, level, str);
    }



void main(void)
{

struct openthread_context *otJanContext;
uint8_t ret;
uint8_t err;
int sock;
const char *proto;
uint8_t buffer[1028];


proto = "DTLS Client";

mbedtls_ssl_conf_dbg(&_ssl_conf, my_debug, NULL);
mbedtls_debug_set_threshold(4);

//start openthread
otJanContext = openthread_get_default_context();
if (otJanContext  == NULL) {
	LOG_INF("couldn't get context");
}

ret = openthread_start(otJanContext);

if (ret > 0) {
	LOG_INF("start did not work, because of %d", ret);
}


//initialize PSK
#if defined(CONFIG_MBEDTLS_KEY_EXCHANGE_PSK_ENABLED)
	err = tls_credential_add(PSK_TAG,
				TLS_CREDENTIAL_PSK,
				psk,
				sizeof(psk));
	if (err < 0) {
		LOG_ERR("Failed to register PSK: %d", err);
	}
	err = tls_credential_add(PSK_TAG,
				TLS_CREDENTIAL_PSK_ID,
				psk_id,
				sizeof(psk_id) - 1);
	if (err < 0) {
		LOG_ERR("Failed to register PSK ID: %d", err);
	}
#endif

struct sockaddr_in6 addr6;

if (IS_ENABLED(CONFIG_NET_IPV6)) {
		addr6.sin6_family = AF_INET6;
		addr6.sin6_port = htons(PEER_PORT);
		LOG_INF("port: %d", PEER_PORT);
		LOG_INF("addr: %s", CONFIG_NET_CONFIG_PEER_IPV6_ADDR);
		inet_pton(AF_INET6, CONFIG_NET_CONFIG_PEER_IPV6_ADDR,
			  &addr6.sin6_addr);
#if defined(CONFIG_NET_SOCKETS_SOCKOPT_TLS)
	sock = socket(addr6.sin6_family, SOCK_DGRAM, IPPROTO_DTLS_1_2);
#else
	sock = socket(addr6.sin6_family, SOCK_DGRAM, IPPROTO_UDP);
#endif
	if (sock < 0) {
		LOG_ERR("Failed to create UDP socket (%s): %d", proto,
			errno);
		return -errno;
	}
}

#if defined(CONFIG_NET_SOCKETS_SOCKOPT_TLS)
	sec_tag_t sec_tag_list[] = {
		//CA_CERTIFICATE_TAG,
#if defined(CONFIG_MBEDTLS_KEY_EXCHANGE_PSK_ENABLED)
		PSK_TAG,
#endif
	};

	ret = setsockopt(sock, SOL_TLS, TLS_SEC_TAG_LIST,
			 sec_tag_list, sizeof(sec_tag_list));
	if (ret < 0) {
		LOG_ERR("Failed to set TLS_SEC_TAG_LIST option (%s): %d",
			proto, errno);
		ret = -errno;
	}
	LOG_INF("PSK added Jan");

	ret = setsockopt(sock, SOL_TLS, TLS_HOSTNAME,
			 TLS_PEER_HOSTNAME, sizeof(TLS_PEER_HOSTNAME));

    if (ret < 0) {
		LOG_ERR("Failed to set TLS_HOSTNAME option (%s): %d",
			proto, errno);
		ret = -errno;
	}
	//added by Jan
	//const char *interface_name = "openthread";
	//ret = setsockopt(data->udp.sock, SOL_SOCKET, SO_BINDTODEVICE, interface_name, strlen(interface_name));

    //ret = fcntl(sock,O_NONBLOCK);

    if (ret < 0) {
		LOG_ERR("Failed to set O_NONBLOCK (%s): %d",
			proto, errno);
		ret = -errno;
	}
#endif
/* Call connect so we can use send and recv. */
	ret = connect(sock, &addr6, sizeof(addr6));
	if (ret < 0) {
		LOG_ERR("Cannot connect to UDP remote (%s): %d", proto,
			errno);
		ret = -errno;
	}

	uint8_t payload[] = "";
	struct coap_packet request;
	const char * const *p;
	uint8_t *coap_data;
	
 	coap_data = (uint8_t *)k_malloc(MAX_COAP_MSG_LEN);
	if (!coap_data) {
        LOG_ERR("Cannot allocate memory for CoAP message (%s): %d", proto,
			errno);
		return -ENOMEM;
	}   
	ret = coap_packet_init(&request, coap_data, MAX_COAP_MSG_LEN,
			     COAP_VERSION_1, COAP_TYPE_CON,
			     COAP_TOKEN_MAX_LEN, coap_next_token(),
			     COAP_METHOD_GET, coap_next_id());
	if (ret < 0) {
		LOG_ERR("Failed to init CoAP message");
        return -1;
	}

	for (p = test_path; p && *p; p++) {
		ret = coap_packet_append_option(&request, COAP_OPTION_URI_PATH,
					      *p, strlen(*p));
		if (ret < 0) {
			LOG_ERR("Unable add option to request");
		}
	}

    ret = send(sock, request.data, request.offset, MSG_WAITALL);

	if (ret < 0 ){
		LOG_INF("error from sending: %d", errno);
	}

	LOG_INF("%s UDP: Sent %d bytes", proto, request.offset);


	while (1) {
		ret = recv(sock, buffer, sizeof(buffer), MSG_DONTWAIT);
		if (ret > 0) {
			LOG_INF("receiving of UDP packet SUCCESSFUL! (%d bytes)",
				ret);
			LOG_HEXDUMP_INF(buffer, ret, "received UDP packet: ");

			return ret;
		}
    }
}


